package com.example.video_appln;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity {

    private Button mrecord,mplay;
    private VideoView mvideoview;
    private final int APPLICATION_CODE=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mrecord=(Button) findViewById(R.id.btnrecord);
        mplay=(Button)findViewById(R.id.btnplay);
        mvideoview=(VideoView)findViewById(R.id.videoview);

        mrecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callvideo=new Intent();
                callvideo.setAction(MediaStore.ACTION_VIDEO_CAPTURE);
                startActivityForResult(callvideo,APPLICATION_CODE);
            }
        });
        mplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mvideoview.start();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==APPLICATION_CODE && resultCode == RESULT_OK){
            Uri videouri=data.getData();
            mvideoview.setVideoURI(videouri);
        }
    }
}
